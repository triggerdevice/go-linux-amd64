ARG GO_VERSION=1.19.2
FROM golang:${GO_VERSION}-buster
LABEL maintainer="mmurase@gmail.com"

RUN apt update && apt-get install -y build-essential libsqlite3-dev

ENV PATH=/go/bin:$PATH \
	CGO_ENABLED=1 \
	GOOS=linux \
	GOARCH=amd64
WORKDIR /go
